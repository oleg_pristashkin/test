<?php

namespace app\controllers;

use Yii;
use yii\web\Controller;
use app\models\ContactForm;
use app\components\TreeHandler;
use app\components\GiphyClient;
use yii\helpers\Url;


class SiteController extends Controller
{

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {

        $model = new ContactForm();
        $treeHandler = new TreeHandler();
        $nodesCount = rand(100, 1000);
        $this->getView()->registerJsFile('js/jquery.js');
        $this->getView()->registerJsVar('ajaxFormUrl', Url::toRoute(['ajaxform']));
        $this->getView()->registerJsFile('js/popup.js');

        return $this->render('index', [
            'tree' => $treeHandler->getRandomTree($nodesCount),
            'model' => $model,
        ]);
    }

    public function actionAjaxform(){
        $model = new ContactForm();
        return $this->render('ajax', [
            'model' => $model,
        ]);
    }

    public function actionImage(){

        if(Yii::$app->request->isPost)
        {
            $giphyClient = new GiphyClient();
            $this->layout = false;
            return $this->render('image', [
                'url' => $giphyClient->getRandomImageUrl()
            ]);
        }

    }

}
