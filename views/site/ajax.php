<?php

use yii\helpers\Html;
use yii\widgets\Pjax;
use yii\bootstrap\ActiveForm;
use yii\helpers\Url;

?>

<?php
$this->registerJs(
    '$("document").ready(function(){
            $("#new_note").on("pjax:end", function() {
            $.pjax.reload({container:"#notes"});  //Reload GridView
        });
    });'
);
?>


</div>
<?php yii\widgets\Pjax::begin(['id' => 'new_note']) ?>

<?php $form = ActiveForm::begin(['action' => Url::toRoute(['site/image']), 'id' => 'contact-form', 'options' => ['data-pjax' => true]]);

?>

<input type="hidden" name="<?= Yii::$app->request->csrfParam; ?>" value="<?= Yii::$app->request->csrfToken; ?>"/>

<div id="notes">
    <?= $form->field($model, 'reCaptcha')->widget(\himiklab\yii2\recaptcha\ReCaptcha2::className(), [
        'name' => 'reCaptcha',
    ]) ?>


    <div class="form-group">
        <?= Html::submitButton('Continue', ['class' => 'btn btn-primary', 'name' => 'contact-button']) ?>
    </div>
</div>


<?php ActiveForm::end(); ?>
<?php Pjax::end(); ?>
