<?php

/* @var $this yii\web\View */

use yii\helpers\Url;
$this->title = 'My Yii Application';
?>
<div class="site-index">

        <div class="popup">
            <a class="close" href="#">&times;</a>
            <div class="content">

            </div>
        </div>

    <div class="body-content">
        <?php
        $func = function($tree) use (&$func){
            echo "<ul>";
            foreach ($tree as $key => $value) {
                echo "<li>";
                echo "<a href='#' id='' class='node'>$key</a>";
                if(is_array($value)){
                    $func($value);
                }
                echo "</li>";
            }
            echo "</ul>";
        };
        $func($tree);
        ?>
    </div>


</div>
