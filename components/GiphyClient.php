<?php

namespace app\components;

use yii\base\BaseObject;

class GiphyClient extends BaseObject
{

    const GIPHY_RANDOM_IMAGE_API_POINT = "http://api.giphy.com/v1/gifs/random?api_key=";
    /**
     * @return string $randomImageUrl
     */
    public function getRandomImageUrl()
    {
        $url = GiphyClient::GIPHY_RANDOM_IMAGE_API_POINT . \Yii::$app->params['giphyApiKey'];

        $object = json_decode(file_get_contents($url));

        return $object->data->images->fixed_height_still->url ?? $object->data->images->fixed_height_still->url ?? "";
    }

}