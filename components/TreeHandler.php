<?php

namespace app\components;

use yii\base\BaseObject;

class TreeHandler extends BaseObject
{
    /**
     * @param integer $nodesCount
     * @return array $tree
     */
    public function getRandomTree(int $nodesCount): array
    {
        $elements = range(0, $nodesCount);
        unset($elements[0]);
        $root = $elements[1];
        unset($elements[1]);
        $this->shuffle_assoc($elements);
        $treeParts = [];

        $startIndex = 2;
        $nodesCount -= $startIndex;
        $nodesCount2 = $nodesCount;

        while ($nodesCount2 > 0) {
            $subArray = [];
            $elem = rand(2, $nodesCount2);

            if ($elem > 10) {
                $elem = 7;
            }

            for ($i = $startIndex; $i < $elem + $startIndex; $i++) {
                $subArray[$i] = $i;
            }
            $startIndex += $elem;
            $nodesCount2 -= $elem;

            $randKey = array_rand($subArray);
            unset($subArray[$randKey]);

            $treeParts[$randKey] = $subArray;
        }


        while (count($treeParts) > 10) {
            $treeParts = $this->addArrayItems($treeParts);
        }

        $result = [$root => $treeParts];

        return $result;
    }

    private function addArrayItems(array $items)
    {
        if (count($items) < 2)
            return $items;

        $randKey2 = array_rand($items);

        $tempElem = $items[$randKey2];

        unset($items[$randKey2]);

        $search = true;

        $tryCounts = 10000;
        while ($search) {
            $tryCounts -= 1;

            if($tryCounts < 0){
                $search = false;
            }

            if (empty($items) )
                $search = false;

            $select = array_rand($items);

            if(empty($items[$select]))
            {
                continue;
            }


            $randKey = array_rand($items[$select]);
            if (is_array($items[$select][$randKey]))
                continue;

            $items[$select][$randKey] = $tempElem;
            $search = false;

        }

        return $items;
    }

    private function shuffle_assoc(&$array)
    {
        $new = [];
        $keys = array_keys($array);
        shuffle($keys);
        foreach ($keys as $key) {
            $new[$key] = $array[$key];
        }
        $array = $new;

        return true;
    }

}